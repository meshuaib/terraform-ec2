data "aws_subnet_ids" "onsite" {
vpc_id = var.vpc_id
}

resource "aws_instance" "onsite" {
  count = "${var.ec2_count}"
  ami           = "${var.ami_id}"
  instance_type = "${var.instance_type}"
 subnet_id = tolist(data.aws_subnet_ids.onsite.ids)[ count.index % length(data.aws_subnet_ids.onsite.ids) ]
  
vpc_security_group_ids = ["${aws_security_group.onsite.id}"]
root_block_device {
    volume_size = "${var.volume_size}"
  }
  tags = {
    Names = "${var.hostname}"
  }
    }